// Toy application representing an MD simulation
#include <cassert>
#include <mui.h>
#include <mpi.h>
#include "md.hpp"

namespace md {
  Simulation::Simulation(MPI_Comm c, int nt) : comm_world(c), steps(nt) {
  }
  template<typename UnifaceT>
  void Simulation::Run(UnifaceT& uni) {
    for (int t = 0; t < steps; ++t) {
      //recv
#ifdef USE_MUI_PARAMS
      uni.barrier(t);
      auto x = uni.template fetch<double>("arg");
#else
      double x = uni.fetch("arg", 0, t, mui::sampler_exact1d<double>(), mui::chrono_sampler_exact1d());
#endif
      // compute
      MPI_Barrier(comm_world);
      double ans = x*x;
      //send
#ifdef USE_MUI_PARAMS
      uni.push("ans", ans);
#else
      uni.push("ans", 0, ans);
#endif
      uni.commit(t);
    }
  }
}

int main(int argc, char* argv[]) {
  MPI_Comm app_comm_world = mui::mpi_split_by_app();

  // Each MD must have exactly one interface, mpi://md/ifN
  // where N is the ID given by the CFD
  auto interfaces = mui::create_uniface<mui::config_1d>("md", {argv[1]});
  
  auto sim = md::Simulation(app_comm_world, 10);
  sim.Run(*interfaces[0]);
  
  return 0;
}
