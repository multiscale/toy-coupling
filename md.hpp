#ifndef MD_HPP
#define MD_HPP

namespace md {
  class Simulation {
    MPI_Comm comm_world;
    double steps;
  public:
    Simulation(MPI_Comm c, int nsteps);
    template<typename UnifaceT>
    void Run(UnifaceT&);
  };
}

#endif
