#ifndef CFD_HPP
#define CFD_HPP

#include <vector>


namespace cfd {
  class Simulation {
    MPI_Comm comm_world;
    double steps;
  public:
    Simulation(MPI_Comm c, int steps);

    template<typename UnifaceT>
    void Run(std::vector<UnifaceT>&);
    
  };
  
}
#endif
