// Toy application representing a CFD simulation
#include <cassert>
#include <mui.h>
#include <mpi.h>
#include <iostream>
#include "cfd.hpp"

namespace cfd {
  Simulation::Simulation(MPI_Comm c, int nsteps) :
    comm_world(c), steps(nsteps) {
  }

  template<typename UnifaceT>
  void Simulation::Run(std::vector<UnifaceT>& interfaces) {
    int rank;
    MPI_Comm_rank(comm_world, &rank);

    for (int t = 0; t < steps; ++t) {
      if (!rank)
	std::cout << "T = " << t << std::endl;
      // push values
      double val = t;
      for(auto i = 0; i < interfaces.size(); ++i) {
	if (!rank)
	  std::cout << "Sending " << val << std::endl;

#ifdef USE_MUI_PARAMS
	interfaces[i]->push("arg", val++);
#else
	interfaces[i]->push("arg", 0, val++);
#endif
	interfaces[i]->commit(t);
      }
      
      // get answers
      std::vector<double> answers(interfaces.size());
      
      for(auto i = 0; i < interfaces.size(); ++i) {
#ifdef USE_MUI_PARAMS
	interfaces[i]->barrier(t);
	answers[i] = interfaces[i]->template fetch<double>("ans");
#else
	answers[i] = interfaces[i]->fetch("ans", 0, t, mui::sampler_exact1d<double>(), mui::chrono_sampler_exact1d());
#endif
	if (!rank)
	  std::cout << "Received " << answers[i] << std::endl;
      }
      
      // compute
      MPI_Barrier(comm_world);
    }
  }
}


int main(int argc, char* argv[]) {
  MPI_Comm app_comm_world = mui::mpi_split_by_app();

  // N interfaces given to talk to MDs (argv[1] ... argv[argc-1])
  std::vector<std::string> if_names;
  if_names.reserve(argc-1);
  for(auto i = 1; i < argc; ++i)
    if_names.emplace_back(argv[i]);

  auto interfaces = mui::create_uniface<mui::config_1d>("cfd", if_names);

  auto sim = cfd::Simulation(app_comm_world, 10);
  sim.Run(interfaces);
  
  return 0;
}
