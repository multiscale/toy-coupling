

### Setup

Download MUI and note the path to it, e.g.:

```
git clone https://github.com/MxUI/MUI.git
set MUI_DIR=$PWD/MUI
```

Load any modules, e.g. on Cirrus:

```
module load gcc cmake intel-mpi-18
```

### Compile

Standard CMake, but you have to set the MUI_DIR variable the first
time at least

```
mkdir build
cd build
cmake -DMUI_DIR=$MUI_DIR ..
make
```

### Run

You need to use MPMD lauch syntax when starting.

You must launch exactly one CFD executable and N >= 1 MD executables.

The CFD executable needs N interface names it will use to connect to
the MD simulations.

You must launch N MD executables and each needs the name of the
interface it will use to talk to the CFD.

e.g. for N = 2:

```
mpirun -np 4 ./cfd if0 if1 : -np 1 ./md if0 : -np 2 ./md if1
```

